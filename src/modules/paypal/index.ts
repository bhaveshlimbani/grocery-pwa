
import { module } from './store'
import { beforeRegistration } from './hooks/beforeRegistration'
import { afterRegistration } from './hooks/afterRegistration'
import { StorefrontModule } from 'core/lib/modules';

export const KEY = 'payment-paypal-magento2'

export const PaypalModule: StorefrontModule = function ({store, appConfig}) {
  store.registerModule('paypal', module)
  beforeRegistration(appConfig, store)
  afterRegistration(appConfig, store)
}
